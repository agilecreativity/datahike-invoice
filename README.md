# datahike-invoice

This is a simple demonstration repository to explore datalog with
[datahike](https://github.com/replikativ/datahike) to create an invoice in PDF
form with the help of [selmer](https://github.com/yogthos/Selmer/) and pdflatex.

## Usage

Walk through the core namespace manually.

## License

Copyright © 2018 Christian Weilbach

Distributed under the MIT License.
