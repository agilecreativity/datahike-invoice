(ns datahike-invoice.core
  (:require [datahike.api :as d]
            [selmer.parser :refer [render-file]]
            [selmer.filters :refer [add-filter!] :as filters]
            [clojure.java.shell :as sh]
            [com.rpl.specter :as S]))



(def uri "datahike:file:///tmp/invoicing")


(def schema {:task-group/tasks {:db/cardinality :db.cardinality/many
                                :db/valueType   :db.type/ref}
             :offer/task-groups {:db/cardinality :db.cardinality/many
                                 :db/valueType :db.type/ref}
             :offer/name {:db/cardinality :db.cardinality/one
                          :db/unique :db.unique/identity}
             :customer/name {:db/cardinality :db.cardinality/one
                             :db/unique :db.unique/identity}})



(comment
  ;; (re)create database
  (d/delete-database uri)
  (d/create-database-with-schema uri schema)
  )


(def conn (d/connect uri))


(d/transact conn [{:db/id               (d/tempid -1)
                   :customer/department "IT Services"
                   :customer/name       "FunkyHub Startup"
                   :customer/contact    ""
                   :customer/street     "Hauptstraße 129"
                   :customer/postal     "62811"
                   :customer/city       "Heidelberg"
                   :customer/country    "Germany"}
                  {:db/id               (d/tempid -1)
                   :customer/department ""
                   :customer/name       "Schmitt GmbH"
                   :customer/contact    "Peter Schmitt"
                   :customer/street     "Heinrich-Heine-Str. 182"
                   :customer/postal     "38312"
                   :customer/city       "Heilbronn"
                   :customer/country    "Germany"}])


;; retrieve clients

(d/q '[:find ?e ?on
       :where
       [?e :customer/name ?on]]
     @conn)


(let [task-group-id (d/tempid -1)
      task-ids (vec (for [i (range 2)] (d/tempid -1)))]
  (d/transact conn [{:db/id             (task-ids 0)
                     :task/name         "Adjustment Login"
                     :task/effort       1
                     :task/effort-unit  :hour
                     :task/effort-price 75
                     :task/price-unit   :euro}
                    {:db/id             (task-ids 1)
                     :task/name         "Extend database schema"
                     :task/effort       4
                     :task/effort-unit  :hour
                     :task/effort-price 75
                     :task/price-unit   :euro}
                    {:db/id             task-group-id
                     :task-group/name  "Android App"
                     :task-group/tasks  task-ids}
                    {:db/id             (d/tempid -1)
                     :offer/customer    [:customer/name "FunkyHub Startup"]
                     :offer/name        "Adjustments App Q1 2018"
                     :offer/advisor     "Christian Weilbach"
                     :offer/task-groups [task-group-id]}
                    ]))

(d/q '[:find ?e ?on
       :where
       [?e :offer/name ?on]]
     @conn)


(d/q '[:find (pull ?tg [*])
       :where
       [?tg :task-group/name ?tgn]]
     @conn)





;; example for a nested pull expression

(def offer-map
  (d/pull @conn '[* {:offer/task-groups [:task-group/name {:task-group/tasks [*]}]}]
          6))



;; setup selmer

(def i18n {:hour "hour"
           :day "day"})

(defn translate [e]
  [:safe (get i18n e (str "NOT-FOUND: " e))])


(def units {:euro "\\euro{}"
            :british-pound "\\textsterling"})

(defn unit [e]
  [:safe (get units e (str "NOT-FOUND: " e))])

(defn price-format [e]
  [:safe (String/format java.util.Locale/UK "%.2f" (into-array [(double e)]))])

(defn effort-format [e]
  [:safe (String/format java.util.Locale/UK "%.1f" (into-array [(double e)]))])

(add-filter! :translate translate)

(add-filter! :unit unit)

(add-filter! :pf price-format)

(add-filter! :ef effort-format)

(comment
  (filters/call-filter :ef 2.0)

  (filters/remove-filter! :ef))


(defn render-pdf [{:keys [client id-prefix name-prefix footer offer-id]}]
  (let [eid (d/q '[:find ?e .
                   :in $ ?client
                   :where
                   [?e :customer/name ?client]]
                 @conn
                 client)
        oid offer-id ;; 7 for emotrack, 25 for 100worte
        offer (d/pull @conn '[* {:offer/task-groups
                                 [:task-group/name {:task-group/tasks [*]}]}]
                      oid)
        client (d/pull @conn '[*] eid)

        offer-map (merge client (assoc offer :offer/id oid))
        offer-map (S/transform [:offer/task-groups S/ALL :task-group/tasks S/ALL]
                               (fn [{:keys [task/effort task/effort-price] :as task}]
                                 (assoc task :task/total-effort (* effort effort-price)))
                               offer-map)
        offer-map (S/transform [:offer/task-groups S/ALL]
                               (fn [{:keys [task-group/tasks] :as task-group}]
                                 (assoc task-group

                                        :task-group/price
                                        (reduce + (map :task/total-effort tasks))

                                        :task-group/price-unit
                                        (:task/price-unit (first tasks))))
                               offer-map)
        total-price (reduce + (map :task-group/price (:offer/task-groups offer-map)))
        tax-rate 19
        tax (* (Math/ceil (* tax-rate total-price)) 0.01)
        offer-map (assoc offer-map
                         :offer/name-prefix name-prefix
                         :offer/id-prefix id-prefix
                         :offer/footer footer
                         :offer/total-price total-price
                         :offer/tax tax
                         :offer/price-unit (:task-group/price-unit
                                            (first (:offer/task-groups offer-map)))
                         :offer/brutto (+ tax total-price))]

    (spit "resources/out.tex" (render-file "invoice.tex" offer-map))
    (sh/sh "pdflatex" "out.tex" :dir "resources")))




(render-pdf {:client "FunkyHub Startup"
             :name-prefix "Invoice for "
             :id-prefix "Your Invoice-Nr."
             :footer "The invoice is for the work done in May 2018. The payment is due in 7 days from invoice date."
             :offer-id 6})

